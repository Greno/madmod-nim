import matplotlib.pyplot as pl

def nimsum(a, b):
    return a^b

def betyg(blist):
    n = 0
    while True:
        if n not in blist:
            return n
        n += 1

sparadebetyg = {}

def betygrec(n):
    if (n<=3): return n
    elif n in sparadebetyg.keys():
        return sparadebetyg[n]
    else :
        blist = [betygrec(n-3),betygrec(n-2),betygrec(n-1)]
        sparadebetyg[n] = betyg(blist)
        return sparadebetyg[n]

result = []
for i in range(0,40):
    result.append(betygrec(i))

pl.yticks([0,1,2,3])
pl.xlabel("Element")
pl.ylabel("Betyg")
pl.grid(True, axis = 'y')
pl.step(list(range(0, 40)), result)
pl.show()