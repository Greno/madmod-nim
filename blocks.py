import matplotlib.pyplot as pl

def nimsum(a, b):
    return a^b

def betyg(blist):
    n = 0
    while True:
        if n not in blist:
            return n
        n += 1

sparadebetyg = {}

def betygrec(n):
    if (n<2): return 0
    if (n==2): return 1
    
    if n in sparadebetyg.keys():
        return sparadebetyg[n]
    else:
        blist = []
        for i in range(0,n-2):
            left = i
            right = n-2 - i
            blist.append(nimsum(betygrec(left),betygrec(right)))
        sparadebetyg[n] = betyg(blist)
    
    return betyg(blist)

result = []
for i in range(0,400):
    result.append(betygrec(i))

pl.yticks([0,1,2,3,4,5,6,7,8,9])
pl.xlabel("Element")
pl.ylabel("Betyg")
pl.grid(True, axis = 'y')
pl.step(list(range(0, 400)), result, linewidth=0.7)
pl.show()
pl.show()